import subprocess
import argparse
from plProgram import Fact, Rule, search_for_similar_atoms, PlProgram, generate_pl_programs, \
    generate_prolog_code, write_prolog_code_to_file
from sentence import Sentence
from ui import generate_ui
import re

TEST_SENTENCES = [
    "The approval mark shall be clearly legible and be indelible.",
    "The activated system shall comply with traffic rules relating to the DDT in the country of operation.",
    "The activated system shall avoid a collision with an unobstructed crossing pedestrian in front of the vehicle.",
    "After an emergency manoeuvre is terminated the system shall continue to operate.",
    "In case of an unplanned event, a transition demand shall be given upon detection.",
    "During the transition phase the system shall continue to operate.",
    "The system shall be deactivated at the end of any minimum risk manoeuvre.",
]

TEST_SENTENCES22 = [
    "If it's raining, the car should not drive.",
    "If it's raining, the car should drive.",
]


def remove_duplicates(pl_programs: [PlProgram]):
    clean_pl_programs: [PlProgram] = []
    for pl_program in pl_programs:
        program_string = ""
        if pl_program.type == 1:
            # I am not joking
            pl_program.__class__ = Fact
            fact: Fact = pl_program
            program_string = fact.get_string()

        if pl_program.type == 2:
            # I am not joking
            pl_program.__class__ = Rule
            rule: Rule = pl_program
            program_string = rule.get_string()

        # check if program already exists:
        new_program = True
        for clean_program in clean_pl_programs:
            if clean_program.get_string() == program_string:
                new_program = False
        if new_program:
            clean_pl_programs.append(pl_program)
    return clean_pl_programs


# connected_requirements =  If a sentence has no subject, get it from the previous requirement.
#                           This only works if requirements are connected
def main(requirements: [], similarity_goal=1, connected_requirements=False, validate=False, check_instructions=False):
    pl_programs = []
    connected_subject = ""
    for requirement in requirements:
        # remove all brackets
        requirement = re.sub("[\(\[].*?[\)\]]", "", requirement)

        # add subject from last sentence if wished
        sentence: Sentence
        if connected_requirements:
            sentence = Sentence(requirement, connected_subject)
            connected_subject = sentence.get_subject()
        else:
            sentence = Sentence(requirement)
        for pl_program in generate_pl_programs(sentence):
            pl_programs.append(pl_program)

    # combine similar atoms if wished
    search_for_similar_atoms(similarity_goal, pl_programs)
    # remove duplicate pl_programs
    pl_programs = remove_duplicates(pl_programs)

    prolog_code = generate_prolog_code("", pl_programs)
    write_prolog_code_to_file("prolog_code.pl", prolog_code)

    print(prolog_code)

    if validate:
        subprocess.run(["clingo", "0", "prolog_code.pl", "--warn", "no-atom-undefined"])

    if check_instructions:
        generate_ui(pl_programs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="input file with requirements", type=str)
    parser.add_argument("-v", "--validate", action="store_true",
                        help="This will additionally validate the generated prolog code with the SAT-Solver Clingo.\n "
                             "For this, clingo must be installed on your operating system")
    parser.add_argument("-ci", "--check_instructions", action="store_true",
                        help="This will additionally create a pop-up, that lets you select conditions.\n Based on "
                             "those conditions, the required instructions will be shown.\n For this, swipl must be "
                             "installed on your operating system.")
    parser.add_argument("-cr", "--connected_requirements", action="store_true",
                        help="If requirements are from the same document and in the right order, the tool is able to "
                             "generate better prolog code.\n If a sentence has for example no subject, the subject "
                             "from the previous sentence will be used.")
    parser.add_argument("-s", "--similarity_goal", type=float, default=1.0, metavar='',
                        help="Default is 1. The range is between 0.2-1.0. If the similarity between to atoms is "
                             "higher than the goal, then those atoms will be combined.\n A similarity in the range of "
                             "0.8-0.9 is recommended.\n If the similarity_goal is set to 1, no similarity will be "
                             "checked.")
    args = parser.parse_args()

    requirements = open(args.input, "r")
    requirements = requirements.readlines()
    if len(requirements) == 0:
        print("ERROR: No valid input file!")

    elif 0.2 > args.similarity_goal or 1.0 < args.similarity_goal:
        print("ERROR: similarity_goal needs to be between 0.2 and 1.0!")
    else:
        main(requirements, similarity_goal=args.similarity_goal, connected_requirements=args.connected_requirements,
             validate=args.validate, check_instructions=args.check_instructions)
