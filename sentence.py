import os
import spacy
from spacy.tokens import Span


# load nlp model
output_dir = os.getcwd()
output_dir = output_dir + "/ner_model"
nlp = spacy.load(output_dir)
print("Loaded from", output_dir)


def sort_tree_to_list_(root, new_list, stop_at_tokens=[]):
    new_list.append(root)
    for child in root.children:
        skip = False
        for stop in stop_at_tokens:
            if child.text == stop.text and child.i == stop.i:
                skip = True
                break
        if skip:
            continue
        sort_tree_to_list_(child, new_list)


def sort_tree_to_list(root, skip_token=[]):
    new_list = []
    sort_tree_to_list_(root, new_list, skip_token)
    new_list.sort(key=lambda token: token.i)
    return new_list


def token_list_to_text(token_list, lemmatize=False):
    text = ""
    for token in token_list:
        if lemmatize:
            text = text + token.lemma_ + " "
        else:
            text = text + token.text + " "
    return text[:-1]


def clean_noun_chunks(noun_chunk, lemma=False, det=False):
    cleaned_chunk = ""
    unnecessary_det = ["the", "an", "a"]
    for token in noun_chunk:
        if token.dep_ != "det" or det:
            if token.dep_ == "det" and token.text in unnecessary_det:
                continue
            if lemma:
                cleaned_chunk = cleaned_chunk + token.lemma_ + " "
            else:
                cleaned_chunk = cleaned_chunk + token.text + " "

        # if object has some additional information that is not too long, it should be added to the object
        # instead of standing by its own
        for child in token.children:
            if child.dep_ == "prep":
                prep_list = sort_tree_to_list(child)
                if len(prep_list) > 3:
                    continue
                prep_text = token_list_to_text(prep_list, True)
                cleaned_chunk = cleaned_chunk + prep_text
    if cleaned_chunk[-1] == " ":
        cleaned_chunk = cleaned_chunk[:-1]
    return cleaned_chunk


class Sentence:
    def __init__(self, text: str, connected_subject=''):
        self.doc = nlp(text)
        self.text: str = text
        self.connected_subject = connected_subject
        self.connected_subject = self.get_subject()
        self.sentence_parts: [SentencePart] = self.generate_sentence_parts()

    def get_root(self):
        for token in self.doc:
            if token.dep_ == "ROOT":
                return token

    def get_subject(self):
        for chunk in self.doc.noun_chunks:
            if chunk.root.dep_ == "nsubj" and chunk.root.head.dep_ == "ROOT":
                return clean_noun_chunks(chunk)
        return self.connected_subject

    # If the is no instruction, make one. This is for Error Correcting, as ent model does not always find an instruction
    def get_ents(self):
        found_no_ins = True
        for ent in self.doc.ents:
            if ent.label_ == "INS":
                found_no_ins = False

        returned_ents = []
        new_ins = None
        if found_no_ins:
            new_ins = self.get_root()
            new_span = Span(self.doc, start=new_ins.i, end=new_ins.i, label='INS')
            returned_ents.append(new_span)
        for ent in self.doc.ents:
            if found_no_ins and ent.start == new_ins.i:
                continue
            append_ent = True
            # This happens often and and fixes a error in the current model
            if ent[0].pos_ == "AUX":
                for ancestor in ent[0].ancestors:
                    if ancestor.pos_ == "VERB":
                        better_ent = Span(self.doc, start=ancestor.i, end=ancestor.i, label=ent.label_)
                        append_ent = False
                        returned_ents.append(better_ent)
                        break
            if append_ent:
                returned_ents.append(ent)
        return returned_ents

    def generate_sentence_parts(self):
        sentence_parts = []
        for ent in self.get_ents():
            # Text, that has one of the stop words as root does no longer belong to the current variable
            stop_words = []
            for stop_word in self.get_ents():
                stop_words.append(stop_word.root)

            # get only the the text, that belongs to the variable
            token_list = sort_tree_to_list(ent.root, stop_words)
            sentence_part_text = token_list_to_text(token_list)

            # add connected subjects only to instructions
            if ent.label_ == "INS" and self.connected_subject != '':
                sentence_parts.append(SentencePart(sentence_part_text, ent.label_, self.connected_subject))
            elif ent.label_ == "CON" or ent.label_ == "INS":
                sentence_parts.append(SentencePart(sentence_part_text, ent.label_))
        return sentence_parts


class SentencePart:
    def __init__(self, text: str, part_type, connected_subject=''):
        self.doc = nlp(text)
        self.text: str = text
        self.type = part_type
        self.connected_subject = connected_subject

    def get_root(self):
        for token in self.doc:
            if token.dep_ == "ROOT":
                return token

    def get_predicate(self):
        for child in self.get_root().children:
            if child.dep_ == "advmod" or child.dep_ == "xcomp" or child.dep_ == "ccomp" or child.dep_ == "acomp":
                if self.get_root().text == "is" or self.get_root().text == "be":
                    return child.text
                return self.get_root().text + " " + child.text
        if self.get_root().text.lower() == "in":
            for child in self.get_root().children:
                if child.text.lower() == "case":
                    return self.get_root().text + " " + child.text
        return self.get_root().text

    def get_subject(self):
        for chunk in self.doc.noun_chunks:
            if chunk.root.dep_ == "nsubj" and chunk.root.head.dep_ == "ROOT":
                return clean_noun_chunks(chunk)
            if chunk.root.dep_ == "nsubjpass" and chunk.root.head.dep_ == "ROOT":
                if self.connected_subject != '':
                    return self.connected_subject
                else:
                    return clean_noun_chunks(chunk)
        return self.connected_subject

    def get_objects(self):
        objects = []
        unnecessary_general = ["it"]
        for chunk in self.doc.noun_chunks:
            if chunk.text in self.get_predicate() or self.get_predicate() in chunk.text \
                    or chunk.text in unnecessary_general:
                continue
            if (chunk.root.dep_ in ["nsubj", "nsubjpass"] and chunk.root.head.dep_ != "ROOT") or \
                    chunk.root.dep_ == "pobj" or chunk.root.dep_ == "dobj":
                object_to_append = clean_noun_chunks(chunk, True, True)
                new_object = True
                for object in objects:
                    if object in object_to_append or object_to_append in object:
                        new_object = False
                if new_object:
                    objects.append(object_to_append)
        for child in self.get_root().children:
            if child.dep_ == "prep":
                prep_list = sort_tree_to_list(child)
                prep_text = token_list_to_text(prep_list, True)

                # Checks if object already exists in object list
                new_object = True
                for object in objects:
                    if object in prep_text or prep_text in object:
                        new_object = False
                if new_object:
                    objects.append(prep_text)
        return objects

    def get_adjective(self):
        for child in self.get_root().children:
            if child.dep_ == "advmod":
                return child.lemma_
        return ''

    def is_negation(self):
        for child in self.get_root().children:
            if child.dep_ == "neg" or (child.dep_ == "det" and child.text.lower() == "no"):
                return True
        return False
