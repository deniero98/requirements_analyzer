# Requirements Analyzer
This tool will create prolog code based on requirements written in natural language

_in order to use the full functionality, swipl and clingo need to be installed on your operating system_

## install python requirements:
_depending on your operating system, python might needs to be substituted for python3_

```console
python -m venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage:
### python main.py test_sentences.txt
This will generate prolog code based on test_sentences.txt

### python main.py test_sentences.txt --validate
This will additionally validate the generated Prolog code with the SAT-Solver Clingo. 
Clingo must be installed on your operating system.

### python main.py test_sentences.txt --check_instructions
 This flag will create a pop-up, that lets you select conditions. 
 Based on those conditions, the required instructions will be shown. 
 Swipl must be installed on your operating system.

### python main.py test_sentences.txt --similarity_goal 0.2-1
The default is 1. 
If the similarity between two atoms is higher than the goal, those atoms will be combined. 
A similarity in the range of 0.8-0.9 is recommended. 
If the similarity goal is set to 1, no similarity will be checked.

### python main.py test_sentences.txt --connected_requirements
This flag should be set, if the input sentences are from the same document and in the correct order. 
The tool will then be able to generate a better Prolog code. 
If a sentence has for example no subject, the subject from the previous sentence will be used. 
This way, the program can take some of the structure of the requirements into account.