import random
import subprocess
import pandas
import PySimpleGUI as sg
from plProgram import CompoundTerm, PlProgram, Fact, find_all_conditions, find_all_instructions, generate_prolog_code, \
    write_prolog_code_to_file


class Checkbox:
    def __init__(self, compound_term: CompoundTerm, default=False):
        self.key: int = random.randint(0, 100000)
        self.compound_term = compound_term
        self.default = default


class InstructionUi:
    def __init__(self, compound_term: CompoundTerm, default=False):
        self.compound_term = compound_term
        self.default = default


def create_condition_window(conditions: [Checkbox]):
    layout = []
    for condition in conditions:
        # instructions should not be able to be changed
        # TODO: If negations are added, this needs to be changed
        if condition.default:
            layout.append([sg.Checkbox(condition.compound_term.get_string(), default=condition.default, key=str(condition.key), disabled=True)])
        else:
            layout.append([sg.Checkbox(condition.compound_term.get_string(), default=condition.default, key=str(condition.key))])

    layout.append([sg.Button('Show instructions')])

    # Create the window and read for input
    window = sg.Window("Select Conditions", layout)
    event, values = window.read()

    return_selected_checkboxes = []
    for condition in conditions:
        if values[str(condition.key)]:
            condition.default = True
        return_selected_checkboxes.append(condition)

    window.close()
    return return_selected_checkboxes


def create_instruction_overview(instructions: [InstructionUi]):
    layout = []
    for instruction in instructions:
        if instruction.default:
            layout.append([sg.Text(instruction.compound_term.get_string() + " = " + str(instruction.default),
                                   text_color="#049712")])
            layout.append([sg.Text("\t" + instruction.compound_term.sentence.text, text_color="#049712")])
        else:
            layout.append([sg.Text(instruction.compound_term.get_string() + " = " + str(instruction.default),
                                   text_color="#C70039")])
            layout.append([sg.Text("\t" + instruction.compound_term.sentence.text, text_color="#C70039")])

    layout.append([sg.Button("Go back"), sg.Button('Finish')])

    window = sg.Window("Show Instructions", layout)
    event, values = window.read()
    window.close()

    if event == "Go back":
        return True
    return False


def generate_checkboxes(pl_programs: [PlProgram]):
    checkboxes = []
    check_duplicates = []

    # include instructions without conditions
    for pl_program in pl_programs:
        if pl_program.type == 1:
            # I am not joking
            pl_program.__class__ = Fact
            fact: Fact = pl_program
            check_duplicates.append(fact.compound_term)
            checkboxes.append(Checkbox(fact.compound_term, True))

    # include all conditions
    conditions = find_all_conditions(pl_programs)
    for condition in conditions:
        # check for duplicates:
        new_compound = True
        for duplicate in check_duplicates:
            if duplicate.get_string() == condition.get_string():
                new_compound = False
        if new_compound:
            checkboxes.append(Checkbox(condition))
            check_duplicates.append(condition)
    return checkboxes


def generate_ui(pl_programs: [PlProgram]):
    sg.theme('Material 1')

    show_ui_again = True
    while show_ui_again:
        # write only rules
        prolog_code_ui = generate_prolog_code("", pl_programs, facts=False, swipl=True)

        # append all facts based on checkboxes
        facts_to_append = []
        selected_checkboxes = create_condition_window(generate_checkboxes(pl_programs))
        for selected in selected_checkboxes:
            facts_to_append.append(Fact(selected.compound_term, selected.default))

        prolog_code_ui = generate_prolog_code(prolog_code_ui, facts_to_append, rules=False, swipl=True)

        write_prolog_code_to_file(".ui_check_instructions_prolog_code.pl", prolog_code_ui)

        # execute code will all instructions
        instruction_ui = []
        for instruction in find_all_instructions(pl_programs):
            exitcode = subprocess.run(["swipl", "-q", "-f", ".ui_check_instructions_prolog_code.pl", "-t", instruction.get_string() + '.'])
            if exitcode.returncode == 0:
                instruction_ui.append(InstructionUi(instruction, True))
            else:
                instruction_ui.append(InstructionUi(instruction, False))
        show_ui_again = create_instruction_overview(instruction_ui)