from typing import List
import random
from sentence import Sentence, SentencePart, nlp


class Atom:
    def __init__(self, name: str):
        self.name: str = name
        self.id: int = random.randint(0, 100000)

    def get_real_name(self):
        return self.name.replace(' ', '_').lower()


class CompoundTerm:
    def __init__(self, functor: Atom, arguments: [], sentence: SentencePart = None):
        self.functor: Atom = functor
        self.arguments: List[CompoundTerm] = arguments
        self.sentence: SentencePart = sentence

    def get_string(self):
        string_to_return = self.functor.get_real_name()

        for i, argument in enumerate(self.arguments):
            if i == 0:
                string_to_return = string_to_return + "("

            if i == len(self.arguments) - 1:
                string_to_return = string_to_return + argument.get_string() + ")"
            else:
                string_to_return = string_to_return + argument.get_string() + ", "
        return string_to_return

    def get_id(self):
        compound_id = self.functor.id
        for argument in self.arguments:
            compound_id = compound_id + argument.get_id()
        return compound_id


# Program Type 1 == Fact | Type 2 == Rule
class PlProgram:
    def __init__(self, program_type):
        self.type = program_type


class Fact(PlProgram):
    def __init__(self, compound_term, value: bool = True):
        super().__init__(1)

        self.compound_term: CompoundTerm = compound_term
        self.value = value

    def get_string(self):
        if self.value:
            return self.compound_term.get_string() + "."
        else:
            return ":- " + self.compound_term.get_string() + "."

    def get_string_swipl(self):
        if self.value:
            return self.compound_term.get_string() + "."
        else:
            return self.compound_term.get_string() + " :- false."

    def get_id(self):
        return self.compound_term.get_id()


class Rule(PlProgram):
    def __init__(self, left: CompoundTerm, right: [CompoundTerm], value: bool = True):
        super().__init__(2)

        self.left: CompoundTerm = left
        self.right: [CompoundTerm] = right
        self.value = value

    def get_string(self):
        if self.value:
            return self.get_string_swipl()
        else:
            string_to_return = ":- " + self.left.get_string()
            if len(self.right) == 0:
                string_to_return = string_to_return + "."
            else:
                string_to_return = string_to_return + ", "

            for i, compound_term in enumerate(self.right):
                if i == len(self.right) - 1:
                    string_to_return = string_to_return + compound_term.get_string() + "."
                else:
                    string_to_return = string_to_return + compound_term.get_string() + ", "
            return string_to_return

    def get_string_swipl(self):
        string_to_return = self.left.get_string() + " :- "
        for i, compound_term in enumerate(self.right):
            if i == len(self.right) - 1:
                string_to_return = string_to_return + compound_term.get_string() + "."
            else:
                string_to_return = string_to_return + compound_term.get_string() + ", "
        return string_to_return

    def get_id(self):
        program_id = self.left.get_id()
        for compound in self.right:
            program_id = program_id + compound.get_id()
        return program_id


def find_all_conditions(pl_programs: [PlProgram]):
    conditions = []
    for pl_program in pl_programs:
        if pl_program.type == 2:
            # I am not joking
            pl_program.__class__ = Rule
            rule: Rule = pl_program
            for compound_term in rule.right:
                conditions.append(compound_term)
    return conditions


def find_all_instructions(pl_programs: [PlProgram]):
    instructions = []
    for pl_program in pl_programs:
        new_instruction: CompoundTerm = None
        if pl_program.type == 1:
            # I am not joking
            pl_program.__class__ = Fact
            fact: Fact = pl_program
            new_instruction = fact.compound_term
        if pl_program.type == 2:
            # I am not joking
            pl_program.__class__ = Rule
            rule: Rule = pl_program
            new_instruction = rule.left
        new_instruction_bool = True
        for instruction in instructions:
            if instruction.get_string() == new_instruction.get_string():
                new_instruction_bool = False
        if new_instruction_bool:
            instructions.append(new_instruction)
    return instructions


def find_similar(atom: Atom, to_compare, goal):
    if goal == 1:
        return atom, False

    best_sim = -1
    best_atom = atom
    word_doc = nlp(atom.name)
    for compare_atom in to_compare:
        prolog_doc = nlp(compare_atom.name)
        similarity = prolog_doc.similarity(word_doc)
        # print(word, similarity, prolog_word)
        if similarity > best_sim:
            best_atom = compare_atom
            best_sim = similarity

    if best_sim >= goal:
        #print("Updated " + atom.name + " to " + best_atom.name + "!")
        return best_atom, True
    else:
        return atom, False


def search_for_similar_atoms_(similarity_goal, compound_term: CompoundTerm, atoms: [Atom]):
    atom, changed = find_similar(compound_term.functor, atoms, similarity_goal)
    if changed:
        compound_term.functor = atom
    else:
        atoms.append(atom)

    arguments = []
    for argument in compound_term.arguments:
        argument, atoms = search_for_similar_atoms_(similarity_goal, argument, atoms)
        arguments.append(argument)
    compound_term.arguments = arguments

    return compound_term, atoms


def search_for_similar_atoms(similarity_goal, pl_programs):
    atoms: [Atom] = []
    for pl_program in pl_programs:
        if pl_program.type == 1:
            # I am not joking
            pl_program.__class__ = Fact
            fact: Fact = pl_program
            compound_term, atoms = search_for_similar_atoms_(similarity_goal, fact.compound_term, atoms)
            fact.compound_term = compound_term

        if pl_program.type == 2:
            # I am not joking
            pl_program.__class__ = Rule
            rule: Rule = pl_program
            compound_term, atoms = search_for_similar_atoms_(similarity_goal, rule.left, atoms)
            rule.left = compound_term

            rights = []
            for compound in rule.right:
                compound, atoms = search_for_similar_atoms_(similarity_goal, compound, atoms)
                rights.append(compound)
            rule.right = rights


def generate_compound_term(sentence_part: SentencePart):
    functor = Atom(sentence_part.get_predicate())
    arguments: [CompoundTerm] = []

    # Subject
    if sentence_part.get_subject() != "":
        subject = Atom(sentence_part.get_subject())
        arguments.append(CompoundTerm(subject, []))

    # Object
    for sentence_object in sentence_part.get_objects():
        new_object = Atom(sentence_object)
        arguments.append(CompoundTerm(new_object, []))

    return CompoundTerm(functor, arguments, sentence_part)


def generate_pl_programs(sentence: Sentence):
    conditions = []
    instructions = []
    for sentence_part in sentence.sentence_parts:
        if sentence_part.type == "INS":
            instructions.append(sentence_part)
        elif sentence_part.type == "CON":
            conditions.append(sentence_part)

    # for each instruction, there needs to be a program
    # conditions are on the right side
    right: [CompoundTerm] = []
    for condition in conditions:
        right.append(generate_compound_term(condition))

    pl_programs: [PlProgram] = []
    for instruction in instructions:
        # create fact
        compound_term = generate_compound_term(instruction)
        if instruction.is_negation():
            print(instruction.text)
        if len(right) == 0:
            pl_programs.append(Fact(compound_term, not instruction.is_negation()))
        # create rule
        else:
            pl_programs.append(Rule(compound_term, right, not instruction.is_negation()))
    return pl_programs


def generate_prolog_code(prolog_code: str, pl_programs: [PlProgram], rules=True, facts=True, swipl=False):
    for pl_program in pl_programs:
        if pl_program.type == 1 and facts:
            # I am not joking
            pl_program.__class__ = Fact
            fact: Fact = pl_program
            if swipl:
                prolog_code = prolog_code + fact.get_string_swipl() + "\n"
            else:
                prolog_code = prolog_code + fact.get_string() + "\n"

        elif pl_program.type == 2 and rules:
            # I am not joking
            pl_program.__class__ = Rule
            rule: Rule = pl_program
            if swipl:
                prolog_code = prolog_code + rule.get_string_swipl() + "\n"
            else:
                prolog_code = prolog_code + rule.get_string() + "\n"

    return prolog_code


def write_prolog_code_to_file(filename: str, prolog_code: str, sort=True):
    # write code to file
    f = open(filename, 'w')
    f.write(prolog_code)
    f.close()

    if sort:
        prolog_code_list = []
        with open(filename, 'r') as f:
            for line in f:
                prolog_code_list.append(line)
        prolog_code_list.sort()
        with open(filename, 'w') as f:
            for item in prolog_code_list:
                f.write(item)
